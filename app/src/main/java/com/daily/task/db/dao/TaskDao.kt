package com.daily.task.db.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.daily.task.db.entity.Task
import com.daily.task.db.entity.User

@Dao
interface TaskDao {

    //region User Region

    @Insert
    fun insertUser(user: User) : Long

    @Query("SELECT * FROM USER")
    fun getAllUser() : LiveData<List<User>>

    @Delete
    fun deleteUser(user: User)

    //endregion

    //region Task Region
    @Insert
    fun insertTask(task: Task) : Long

    @Query("SELECT * FROM TASK ORDER BY taskCreatedOn DESC")
    fun getAllTask(): LiveData<List<Task>>

    @Query("SELECT * FROM TASK WHERE taskId = :taskId")
    fun  getTaskById(taskId : Int) : Task

    @Update
    fun updateTask(task : Task)

    @Delete
    fun deleteTask(task: Task)

    //endregion
}