package com.daily.task.db

import android.content.Context
import androidx.room.*
import androidx.room.migration.Migration
import com.daily.task.db.dao.TaskDao
import com.daily.task.db.entity.Task
import com.daily.task.db.entity.User

@Database(
    entities = [User::class, Task::class],
    version = 1
)
@TypeConverters(DataTypeConverters::class)
abstract class TaskDatabase : RoomDatabase() {
    abstract fun provideTaskDao(): TaskDao

    companion object{

        @Volatile
        private var INSTANCE : TaskDatabase ?= null

        fun getInstance(context: Context) : TaskDatabase{
            synchronized(this){
                return INSTANCE ?: buildDatabase(context).also {
                    INSTANCE = it
                }
            }
        }

        private fun buildDatabase(context: Context): TaskDatabase {
            return Room.databaseBuilder(context.applicationContext,
                TaskDatabase::class.java, "task_db")
                .build()
        }

    }
}