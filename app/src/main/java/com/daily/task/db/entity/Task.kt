package com.daily.task.db.entity

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Ignore
import androidx.room.PrimaryKey
import java.util.*

@Entity(foreignKeys = [ (ForeignKey(
    entity = User::class,
    parentColumns = ["userId"],
    childColumns = ["user_id"],
    onDelete = ForeignKey.SET_NULL
)) ])
data class Task (
    @PrimaryKey(autoGenerate = true)
    var taskId : Int,
    var taskTitle : String ,
    var taskNote : String,
    var taskCreatedOn : Date ,
    var user_id : Int ?
){
    @Ignore
    constructor(
        title: String = "",
        note : String = "",
        createdOn : Date = Date(System.currentTimeMillis()),
         user : Int ? = null
    ) : this(0,title, note,createdOn, user)
}