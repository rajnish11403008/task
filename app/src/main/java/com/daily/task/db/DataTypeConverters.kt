package com.daily.task.db

import androidx.room.TypeConverter
import androidx.room.TypeConverters
import java.util.*

class DataTypeConverters {

    @TypeConverter
    fun convertLongToDate(value : Long?) : Date? {
        return if (value == null ) null else Date(value)
    }

    @TypeConverter
    fun convertDateToLong(value : Date? ): Long?{
        return value ?.time
    }

}