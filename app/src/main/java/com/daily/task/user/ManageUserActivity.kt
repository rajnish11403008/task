package com.daily.task.user

import android.app.Dialog
import android.content.DialogInterface
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.widget.TextView
import android.widget.Toolbar
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.daily.task.R
import com.daily.task.adapter.UserAdapter
import com.daily.task.db.TaskDatabase
import com.daily.task.db.dao.TaskDao
import com.daily.task.db.entity.User
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_manage_user.*
import kotlinx.android.synthetic.main.activity_manage_user.view.*
import org.w3c.dom.Text
import kotlin.concurrent.thread

class ManageUserActivity : AppCompatActivity(), UserAdapter.UserItemClickListener {


    private lateinit var taskDatabase: TaskDatabase
    private lateinit var taskDao: TaskDao

    private val adapter = UserAdapter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_manage_user)

        setupToolBar();
        setupDatabase()
        setupDataObserver();
        setupRecyclerView();

        add_user_btn.setOnClickListener {
            val name = username_edit_text.text.toString()
            if(TextUtils.isEmpty(name)){
                showSnackbar("Name Required")
            }
            else{
                val user = User()
                user.userName = name
                thread {
                    val isInserted = taskDao.insertUser(user)
                    runOnUiThread {
                        if (isInserted != -1L) {
                            username_edit_text.setText("")
                            showSnackbar("User Added")
                        }
                        else{
                            showSnackbar("User Not Added")
                        }
                    }
                }
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right)

    }

    private fun setupToolBar() {
        val toolbar = toolbar
        toolbar.title = ""
        setSupportActionBar(toolbar)
        val ivBack = toolbar.back
        ivBack.setOnClickListener {
            onBackPressed()
        }
    }

    private fun setupRecyclerView() {
        user_recycler_view.adapter = adapter
    }

    private fun setupDataObserver() {
        taskDao.getAllUser().observe(this, Observer<List<User>>{
            adapter.submitList(it)
        })
    }

    private fun setupDatabase() {
        taskDatabase = TaskDatabase.getInstance(this)
        taskDao = taskDatabase.provideTaskDao()
    }

    override fun onClick(user: User) {

        val builder = AlertDialog.Builder(this)
        builder.setTitle("Alert")
        builder.setMessage("Are you sure, You want to delete this User ?")
        var  dialog : Dialog ?= null
        builder.setPositiveButton("Ok") { dialogInterface, i ->
            thread {
                taskDao.deleteUser(user)
                runOnUiThread {
                    showSnackbar("User Deleted")
                }
            }
            dialog!!.dismiss()
        }

        builder.setNegativeButton("Cancel") { dialogInterface, i ->
            dialog!!.dismiss()
        }

        dialog = builder.create()
        dialog.setCancelable(false)

        dialog.show()
    }

    private fun showSnackbar(message : String){
        val snackbar = Snackbar.make(add_user_btn, message,Snackbar.LENGTH_SHORT)
        val view = snackbar.view

        view.setBackgroundColor(ContextCompat.getColor(this, R.color.colorAccent))
        snackbar.show()
    }
}
