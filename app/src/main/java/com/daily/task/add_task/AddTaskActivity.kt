package com.daily.task.add_task

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.BaseAdapter
import android.widget.ListAdapter
import androidx.core.content.ContextCompat
import androidx.core.view.get
import androidx.lifecycle.Observer
import com.daily.task.R
import com.daily.task.db.TaskDatabase
import com.daily.task.db.dao.TaskDao
import com.daily.task.db.entity.Task
import com.daily.task.db.entity.User
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_add_task.*
import kotlinx.android.synthetic.main.activity_add_task.toolbar
import kotlinx.android.synthetic.main.activity_add_task.view.*
import kotlin.concurrent.thread

class AddTaskActivity : AppCompatActivity() {

    private lateinit var taskDatabase: TaskDatabase
    private lateinit var taskDao: TaskDao
    private var task_Id: Int = -1
    private var user_Id: Int = -1
    private lateinit var task: Task
    private var userList = mutableListOf<User>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_task)

        taskDatabase = TaskDatabase.getInstance(this)
        taskDao = taskDatabase.provideTaskDao()

        if (intent != null) {
            task_Id = intent.getIntExtra("taskId", -1)
            if (task_Id != -1) {
                thread {
                    task = taskDao.getTaskById(task_Id)
                    user_Id = task.user_id!!
                    runOnUiThread {
                        setUiData()
                    }
                }
            }
        }

        user_tv.setText(String.format(getString(R.string.assigned_user), "None"))

        setupToolbar();

        add_task_btn.setOnClickListener {
            addOrUpdateTask()
        }

        delete_task.setOnClickListener {
            thread {
                taskDao.deleteTask(task)
                runOnUiThread {
                    showSnackbar(getString(R.string.task_deleted))
                    finishActivity()
                }
            }
        }
        val adapter = ArrayAdapter<String>(
            this,
            android.R.layout.simple_spinner_item,
            mutableListOf<String>()
        )

        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line)

        user_spinner.adapter = adapter

        taskDao.getAllUser().observe(this, Observer {
            userList.clear()
            userList.addAll(it)
            adapter.add("Select User")
            adapter.addAll(it.map { it.userName })
            adapter.notifyDataSetChanged()
        })

        user_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                if(p2 > 0) {
                    val name = adapter.getItem(p2)
                    val user = userList.find { it.userName == name }

                    if (user != null) {
                        user_Id = user.userId
                        user_tv.setText(
                            String.format(
                                getString(R.string.assigned_user),
                                user.userName
                            )
                        )

                        addOrUpdateTask()
                    }
                }
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {

            }
        }

    }

    private fun setUiData() {
        task_title_edittxt.setText(task.taskTitle)
        task_note_edittxt.setText(task.taskNote)
        user_tv.setText(String.format(getString(R.string.assigned_user),task.user_id.toString()))
    }

    private fun addOrUpdateTask() {
        val taskTitle = task_title_edittxt.text
        val taskNote = task_note_edittxt.text

        if (taskTitle.isNullOrEmpty()) {
            showSnackbar("Add Title")
            return
        }
        if(user_Id == -1){
            showSnackbar("Define User")
            return
        }

        if (task_Id == -1) {
            val task = Task()
            task.taskTitle = taskTitle.toString()
            task.taskNote = taskNote.toString()
            task.user_id = user_Id
            thread {
                taskDao.insertTask(task)
                runOnUiThread {
                    showSnackbar(getString(R.string.task_added))
                    finishActivity()

                }
            }
        } else {
            task.taskTitle = taskTitle.toString()
            task.taskNote = taskNote.toString()
            task.user_id = user_Id
            thread {
                taskDao.updateTask(task)
                runOnUiThread {
                    showSnackbar(getString(R.string.task_updated))
                    finishActivity()
                }
            }
        }

    }

    private fun finishActivity() {
        Handler().postDelayed({
            finish()
        }, 1000)
    }

    private fun setupToolbar() {
        val toolbar = toolbar
        val deleteTask = toolbar.delete_task

        if (task_Id == -1) {
            toolbar.title = "Add"
            deleteTask.visibility = View.GONE
        } else {
            toolbar.title = "Update"
            deleteTask.visibility = View.VISIBLE

            add_task_btn.setText(getString(R.string.update_task))
        }

        setSupportActionBar(toolbar)
    }

    private fun showSnackbar(message: String) {
        val snackbar = Snackbar.make(add_task_btn, message, Snackbar.LENGTH_SHORT)
        val view = snackbar.view

        view.setBackgroundColor(ContextCompat.getColor(this, R.color.colorAccent))
        snackbar.show()
    }


}
