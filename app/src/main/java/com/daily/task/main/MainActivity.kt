package com.daily.task.main

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.lifecycle.Observer
import com.daily.task.R
import com.daily.task.adapter.TaskAdapter
import com.daily.task.add_task.AddTaskActivity
import com.daily.task.db.TaskDatabase
import com.daily.task.db.dao.TaskDao
import com.daily.task.db.entity.Task
import com.daily.task.user.ManageUserActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), TaskAdapter.TaskItemClickListener {


    private lateinit var taskDatabase: TaskDatabase
    private lateinit var taskDao: TaskDao
    private val adapter = TaskAdapter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        taskDatabase = TaskDatabase.getInstance(this)
        taskDao = taskDatabase.provideTaskDao()

        setupToolbar()
        setupDbObserver()
        setupRecyclerView()

        add_task_btn.setOnClickListener {
            val addTaskIntent = Intent(this, AddTaskActivity::class.java)
            startActivity(addTaskIntent)
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
        }
    }

    private fun setupRecyclerView() {
        task_recycler_view.adapter = adapter
    }

    private fun setupDbObserver() {
        taskDao.getAllTask().observe(this, Observer<List<Task>> {
            adapter.submitList(it)
        })
    }

    private fun setupToolbar() {
        val toolbar = toolbar
        setSupportActionBar(toolbar)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val menuInflater = menuInflater
        menuInflater.inflate(R.menu.menu_home, menu)
        return true
    }

    override fun onClick(task: Task) {
        val addTaskIntent = Intent(this, AddTaskActivity::class.java)
        addTaskIntent.putExtra("taskId", task.taskId)
        startActivity(addTaskIntent)
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when(item.itemId){
            R.id.manage_user_menu ->{
                val userIntent = Intent(this, ManageUserActivity::class.java)
                startActivity(userIntent)
                overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
            }


        }

        return super.onOptionsItemSelected(item)
    }
}
