package com.daily.task.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.daily.task.R
import com.daily.task.db.entity.Task
import kotlinx.android.synthetic.main.task_item_layout.view.*
import java.text.SimpleDateFormat
import java.util.*

class TaskAdapter(var taskItemClickListener: TaskItemClickListener) :
    ListAdapter<Task, TaskAdapter.TaskViewHolder>(DiffUtilClass()) {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskViewHolder {
        return TaskViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.task_item_layout, parent,false)
        )
    }

    override fun onBindViewHolder(holder: TaskViewHolder, position: Int) {
        holder.bind(getItem(position))
        holder.itemView.setOnClickListener {
            taskItemClickListener.onClick(getItem(position))
        }
    }

    class TaskViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {
        var dateFormat : SimpleDateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.US)
        fun bind(task: Task){
            itemView.task_title_text.setText(task.taskTitle)
            itemView.task_updated_on.setText(dateFormat.format(task.taskCreatedOn))
        }
    }

    class DiffUtilClass : DiffUtil.ItemCallback<Task>(){
        override fun areItemsTheSame(oldItem: Task, newItem: Task): Boolean {
            return oldItem.taskId == newItem.taskId
        }

        override fun areContentsTheSame(oldItem: Task, newItem: Task): Boolean {
            return oldItem == newItem
        }

    }

    interface TaskItemClickListener{
        fun onClick(task: Task)
    }
}