package com.daily.task.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.daily.task.R
import com.daily.task.db.entity.User
import kotlinx.android.synthetic.main.item_layout.view.*

class UserAdapter(var userItemClickListener: UserItemClickListener) : ListAdapter<User, UserAdapter.UserViewHolder>(DiffUtilCallBack()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        return UserViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_layout, parent, false)
        )
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        holder.bind(getItem(position))
        holder.itemView.delete_user_btn.setOnClickListener {
            userItemClickListener.onClick(user = getItem(position))
        }
    }


    class UserViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {



        fun bind(user: User) {
            itemView.username_text_view.text = user.userName
        }

    }

    class DiffUtilCallBack : DiffUtil.ItemCallback<User>(){
        override fun areItemsTheSame(oldItem: User, newItem: User): Boolean {
            return oldItem.userId == newItem.userId
        }

        override fun areContentsTheSame(oldItem: User, newItem: User): Boolean {
            return oldItem == newItem
        }

    }

    interface UserItemClickListener{
        fun onClick(user : User)
    }
}